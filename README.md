# goma
goma is a stochastic Go engine written in `python3`

> **go•ma** [ごま]
> noun
> 1. *Sesamum indicum* Sesame plant, produces small oval seeds.

>small seed of far reach <br>
>white flower in summer blooms <br>
>from oval take oil

### Project Motivation:
This project is an outgrowth of [dango](https://github.com/gsobell/dango), with hope of being integrated back it at a later date, when both are functional. The goal isn't to compete with neural-net Go engines, rather to write a passably decent knowledge based engine from scratch

### Project Blueprint:
* `GTP` compatible I/O
* Basic game logic
* Nigiri pre-game (similar to coin toss)
* Basic joseki (openings)
* Game status estimation
* Use interpolation to make game heatmap

goma is already better than most humans (most humans don't know how to play go)

*Note: this project should be currently considered vaporware unless otherwise stated*

If you like this, you might also enjoy [cbonsai](https://gitlab.com/jallbrit/cbonsai), [sabaki](https://github.com/SabakiHQ/Sabaki), [baduk-fortune](https://github.com/gsobell/baduk-fortune), and [haikunator](https://github.com/usmanbashir/haikunator).
